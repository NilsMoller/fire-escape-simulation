﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

/// <summary>
/// Creates a grid graph based on the CGrid created.
/// </summary>
public class AStarGridCreatorUsingCGrid : MonoBehaviour
{
    /// <summary>
    /// The CGrid scriptable object to dictate values like width, height and cell size.
    /// </summary>
    [SerializeField] CGridScriptableObject gridObject;

    /// <summary>
    /// The layer that the people will see as obstacles.
    /// </summary>
    [SerializeField] LayerMask obstacleLayer;

    private void Start()
    {
        // This holds all graph data
        AstarData data = AstarPath.active.data;

        // This creates a Grid Graph
        GridGraph gg = data.AddGraph(typeof(GridGraph)) as GridGraph;

        // Updates graph settings
        gg.rotation = new Vector3(90, 0, 0);
        gg.collision.type = ColliderType.Ray; //For 2D. Without this it takes a much more spacious calculation for the walkable surface and hallways/doors won't become walkable anymore.
        gg.collision.heightCheck = false; //Because we are in 2D, we have no height
        gg.collision.use2D = true;
        gg.center = gridObject.grid.GetWorldPosition(gridObject.grid.Width / 2, gridObject.grid.Height / 2); //Sets the center of the pathfinding grid to the middle of the CGrid.
        gg.nodeSize = gridObject.grid.CellSize; //Make the nodes the same size as the CGrid cell size.
        gg.SetDimensions(int.Parse((gridObject.grid.Width * gridObject.grid.CellSize).ToString()), int.Parse((gridObject.grid.Height * gridObject.grid.CellSize).ToString()), gridObject.grid.CellSize); //Set the pathfinding grid size to the size of the actual grid multiplied by the cell size to account for size changes (still needs to be tested).
        gg.collision.mask = obstacleLayer; //Actually use the mask passed from the editor in the pathfinding grid
        gg.cutCorners = false; //Basically saying that the node only knows the four nodes directly connected to it, not the diagonals to avoid people getting stuck on corners.

        Debug.Log("Created A* graph");

        AstarData.active.Scan();
    }

    private void Update()
    {
        AstarData.active.Scan();
    }
}
