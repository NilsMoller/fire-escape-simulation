﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    [SerializeField] CGridScriptableObject gridObject;
    [SerializeField] GameObject fire;

    private int x, y;
    private int z;

    private int initXup, initYup;
    private int initXdown, initYdown;

    private int initXtight, initYright;
    private int initXleft, initYleft;

    private int initXrightDiagUp, initYrightDiagUp;
    private int initXrightDiagDown, initYrightDiagDown;

    private int initXleftDiagUp, initYleftDiagUp;
    private int initXleftDiagDown, initYleftDiagDown;



    public FireController(CGridScriptableObject grid)
    {
        this.gridObject = grid;
    }

    public void Start()
    {

    }

    public void StartNewFire()
    {
        this.z = 0;
        gridObject.grid.GetRandomGridPosition(out x, out y);
        x = Random.Range(2, gridObject.grid.Width - 3);
        y = Random.Range(2, gridObject.grid.Height - 3);

        initXup = x;
        initYup = y;
        initXdown = x;
        initYdown = y;

        initXtight = x;
        initYright = y;
        initXleft = x;
        initYleft = y;

        initXrightDiagUp = x;
        initYrightDiagUp = y;
        initXleftDiagUp = x;
        initYleftDiagUp = y;

        initXrightDiagDown = x;
        initYrightDiagDown = y;
        initXleftDiagDown = x;
        initYleftDiagDown = y;
        StartFire_x1();
    }

    public void SpawnFire(int x, int y)
    {
        gridObject.grid.SetValue(x, y, 2, fire);
    }

    //THE REASON FOR 3 DIFFERENT METHODS OF STARTING FIRE IS THAT I CANT CALL INVOKES WITH PARAMETERS
    public void StartFire_x1()
    {
        CancelInvoke();
        ExpandFire();
        SpeedFire(1.5f);
    }
    public void StartFire_x2()
    {
        CancelInvoke();
        ExpandFire();
        SpeedFire(0.8f);
    }
    public void StartFire_x4()
    {
        CancelInvoke();
        ExpandFire();
        SpeedFire(0.4f);
    }

    public void StopFire()
    {
        CancelInvoke();
    }

    private void ExpandFire()
    {
        up();
        down();
        right();
        left();
        diagUpRight();
        diagUpLeft();
        diagDownRight();
        diagDownLeft();

    }

    private void SpeedFire(float speed)
    {
        this.z++;
        if (z <= gridObject.grid.Height && z <= gridObject.grid.Width)
        {   
            if (speed == 1.5f)
            {
                Invoke("StartFire_x1", speed);
            }
            else if (speed == 0.8f)
            {
                Invoke("StartFire_x2", speed);
            }
            else
            {
                Invoke("StartFire_x4", speed);
            }
        }
    }

    private void up()
    {
        SpawnFire(initXup, initYup);
        SpawnFire(initXup + 1, initYup);
        SpawnFire(initXup - 1, initYup);
        this.initYup++;

    }
    private void down()
    {
        SpawnFire(initXdown, initYdown);
        SpawnFire(initXdown + 1, initYdown);
        SpawnFire(initXdown - 1, initYdown);
        this.initYdown--;

    }
    private void right()
    {
        SpawnFire(initXtight, initYright);
        SpawnFire(initXtight, initYright + 1);
        SpawnFire(initXtight, initYright - 1);
        this.initXtight++;
    }
    private void left()
    {
        SpawnFire(initXleft, initYleft);
        SpawnFire(initXleft, initYleft + 1);
        SpawnFire(initXleft, initYleft - 1);
        this.initXleft--;

    }
    private void diagUpRight()
    {
        SpawnFire(initXrightDiagUp, initYrightDiagUp);
        SpawnFire(initXrightDiagUp + 1, initYrightDiagUp + 1);
        SpawnFire(initXrightDiagUp - 1, initYrightDiagUp - 1);
        this.initXrightDiagUp++;
        this.initYrightDiagUp++;

    }
    private void diagUpLeft()
    {
        SpawnFire(initXleftDiagUp, initYleftDiagUp);
        SpawnFire(initXleftDiagUp + 1, initYleftDiagUp + 1);
        SpawnFire(initXleftDiagUp - 1, initYleftDiagUp - 1);
        this.initXleftDiagUp--;
        this.initYleftDiagUp++;

    }
    private void diagDownRight()
    {
        SpawnFire(initXrightDiagDown, initYrightDiagDown);
        SpawnFire(initXrightDiagDown + 1, initYrightDiagDown + 1);
        SpawnFire(initXrightDiagDown - 1, initYrightDiagDown - 1);
        this.initXrightDiagDown--;
        this.initYrightDiagDown--;

    }
    private void diagDownLeft()
    {
        SpawnFire(initXleftDiagDown, initYleftDiagDown);
        SpawnFire(initXleftDiagDown + 1, initYleftDiagDown + 1);
        SpawnFire(initXleftDiagDown - 1, initYleftDiagDown - 1);
        this.initXleftDiagDown++;
        this.initYleftDiagDown--;

    }
}
