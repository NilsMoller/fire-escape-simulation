﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "mapHandler", menuName = "Scriptable Objects/Map Handler")]
public class MapHandlerScriptableObject : ScriptableObject
{
    public MapHandler mapHandler;
}
