﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Integer", menuName = "Scriptable Objects/Common Types/Integer")]
public class IntScriptableObject : ScriptableObject
{
    [HideInInspector] public int Value;
}
