﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GOList", menuName = "Scriptable Objects/Common Types/GameObject List")]
public class GameObjectListScriptableObject : ScriptableObject
{
    [HideInInspector] public List<GameObject> List;
}
