﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class LoadPannel : MonoBehaviour
{
    [SerializeField] MapHandlerScriptableObject MapHandlerObjectbttns;
    public GameObject Lpannel;


    //display the files on the system with the dropdown menu
    //todo:this needs to be displayed in the dropdown list
    private string path = "./floorplans/";

    private List<string> GetFileNames()
    {
        List<string> submitfiles = new List<string>();
        string[] tempfiles = Directory.GetFiles(path);
        foreach (string file in tempfiles)
        {
            if(file.Contains(".fsimdata"))
            {
                string temp = file;
                string temp1 = temp.Replace(path, "");
                string temp2 = temp1.Replace(".fsimdata", "");
                submitfiles.Add(temp2);
            }
        }
        return submitfiles;
    }

    public Dropdown dropdown;
    public void LoadFileNames()
    {
        
        List<string> filenames = GetFileNames();
        //add them to the dropdown list
        dropdown.ClearOptions();
        dropdown.AddOptions(filenames);
        
    }


    public CGridScriptableObject gridObject;
    public void LoadButton()
    {
        Debug.Log(dropdown.options[dropdown.value].text);
        string mapname = dropdown.options[dropdown.value].text;
        MapHandlerObjectbttns.mapHandler.LoadMap(mapname);
        ClosePannel();
    }

    public void ClosePannel()
    {
        if (Lpannel != null)
        {
            Lpannel.SetActive(false);
        }
    }
}
