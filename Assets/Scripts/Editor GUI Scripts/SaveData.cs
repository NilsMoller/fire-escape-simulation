﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    //creates a list of mapobjects which will store the material and position of each block
    public int[,] mapObjects;
    public MapStats mapStats;

    public SaveData(MapHandler mapHandler)
    {
        mapObjects = mapHandler.mapObjects;
        mapStats = mapHandler.mapStatistics;
    }
}
